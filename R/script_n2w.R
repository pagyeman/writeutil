#' Turn numeric vector into english word.
#'
#' Function to convert a numeric vector into an english word
#'
#' This function takes a numeric vector and converts into the corresponding
#' english word. The function always rounds to an integer.
#' #' There are options for UK and US spelling and capitalisation of the final text.
#' This function was originally published in:
#' J. Fox. Programmer’s niche: How Do You Spell That Number?.
#' R News, 5(1):51–55, May 2005. URL https://www.r-project.org/doc/Rnews/Rnews_2005-1.pdf
#'
#'
#' @param x a numercial vector.
#' @param billion a character vector defining the spelling.
#' @param capitalise a logical defining whether the first character will be capitalised
#'
#' @return The provided number as an english word.
#'
#' @export
#'


script_n2w <- function(x, billion = c("US", "UK"), capitalise = FALSE) {

  trim <- function(text){
    gsub("(^\ *)|((\ *|-|,\ zero|-zero)$)", "", text)
  }
  makeNumber <- function(x) as.numeric(paste(x, collapse=""))
  makeDigits <- function(x) strsplit(as.character(x), "")[[1]]
  helper <- function(x){
    negative <- x < 0
    x <- abs(x)
    digits <- makeDigits(x)
    nDigits <- length(digits)
    result <- if (nDigits == 1) as.vector(ones[digits])
    else if (nDigits == 2)
      if (x <= 19) as.vector(teens[digits[2]])
    else trim(paste0(tens[digits[1]], "-", ones[digits[2]]))
    else if (nDigits == 3) {
      tail <- makeNumber(digits[2:3])
      if (tail == 0) paste(ones[digits[1]], "hundred")
      else trim(paste(ones[digits[1]], trim(paste("hundred", and)),
                      helper(tail)))
    }
    else {
      nSuffix <- ((nDigits + 2) %/% 3) - 1
      if (nSuffix > length(suffixes) || nDigits > 15)
        stop(paste(x, "is too large!"))
      pick <- 1:(nDigits - 3*nSuffix)
      trim(paste(helper(makeNumber(digits[pick])),
                 suffixes[nSuffix], helper(makeNumber(digits[-pick]))))
    }
    if (billion == "UK"){
      words <- strsplit(result, " ")[[1]]
      if (length(grep("million,", words)) > 1)
        result <- sub(" million, ", ", ", result)
    }
    if (negative) paste("minus", result) else result
  }
  opts <- options(scipen = 100)
  on.exit(options(opts))

  # Sanity check for variables
  if (missing(x))
    stop("You need to provide an number", call. = FALSE)
  if (!is.numeric(x))
    stop("You need to provide an number", call. = FALSE)

  billion <- match.arg(billion)
  if (!(billion %in% c("US", "UK")))
    stop("you need to define american 'US' or british 'UK' spelling")

  and <-  if (billion == "US") "" else "and"

  # make vectors containing the words that are needed to construct numbers in written text
  ones <- c("zero", "one", "two", "three", "four", "five", "six", "seven",
            "eight", "nine")
  teens <- c("ten", "eleven", "twelve", "thirteen", "fourteen", "fifteen",
             "sixteen", " seventeen", "eighteen", "nineteen")
  names(ones) <- names(teens) <- 0:9
  tens <- c("twenty", "thirty", "forty", "fifty", "sixty", "seventy", "eighty",
            "ninety")
  names(tens) <- 2:9
  suffixes <- if (billion == "US")
    c("thousand,", "million,", "billion,", "trillion,")
  else
    c("thousand,", "million,", "thousand million,", "billion,")


  x <- round(x)
  if (length(x) > 1) {
    result <- sapply(x, helper)
    } else {
      result <- helper(x)
    }

  # capitalise first letter
  if (capitalise) {
    result <- paste0(toupper(strsplit(result, "")[[1]][1]),
                     substr(result, 2, nchar(result)))
  }

  return(result)
}
